#!/usr/bin/perl
# create pattern for triangular lattice array of hexagon
# create Vectorwoks Script data and AutoCad Autolisp
#

# use autalisp::draw;
use lib './';
require 'autolisp.pl';
	
open(OUT,">hexpat.vs") || die "cannot open file\n";
open(OUT2,">hexpat.lsp")  || die "cannot open file\n";

&output_VSheader;

# assume latticetype is A (normal lattice)
$latticeType=0;
#print "Type of lattice (A:0(default) or B:1) ? :";
#$latticeType = <STDIN>;
#chop $latticeType;

print "pitch of the lattice (micron) ? :";
$pitch = <STDIN>;
chop $pitch;

if($latticeType ==0) {
	$max_hsize = &pattern_A;
} else {
	$max_hsize = &pattern_B;
}
 
print "size of the hexagon (micron, max=$max_hsize) ? :";
$hexsize = <STDIN>;
print "Size of the whole pattern (x in micron) ? : ";
$sizex = <STDIN>;
chop $sizex;
print "Size of the whole pattern (y in micron) ? : ";
$sizey = <STDIN>;
chop $sizey;

print "conversion factor (default =10) ? ";
	$cnvfactor= <STDIN>;
	chop $cnvfactor;
if(length($cnvfactor)==0) {
	$cnvfactor = 10;
}
print "magnification (default = 1) ? ";
	$mag = <STDIN>;
	chop $mag;
	if(length($mag)==0) {
		$mag = 1;
	}
	$cnvfactor *= $mag;

&hex_pattern2;

$xs = $sizex/2;
$ys = $sizey/2;
print OUT "Rect(-$xs,-$ys,$xs,$ys);\n";
print OUT2 &autolisp::draw::draw_rectangle_autolisp(-$xs,-$ys,$xs,$ys),"\n";

$mm=int($xs/$pitch);
$nn=int($ys/($pitch*sqrt(3)));
if($mm*$pitch+$dx*$hexsize > $xs) {
	$mm--;
}
if($nn*$pitch*sqrt(3)+$dy*$hexsize > $ys) {
	$nn--;
}
$XP=int($pitch*$cnvfactor+0.5);
$YP=int($pitch*$cnvfactor*sqrt(3)+0.5);
$XN=$mm*2+1;
$YN=$nn*2+1;
$xorig=-$mm*$pitch;
$yorig=$nn*$pitch*sqrt(3);
print "RL 0,0,$XP,$YP,$XN,$YN\n";
&output_hex();
print "RE\n";

$mm=int(($xs-$pitch/2)/$pitch);
$nn=int(($ys-$pitch*sqrt(3)/2)/($pitch*sqrt(3)));
if(($mm+1/2)*$pitch+$dx*$hexsize > $xs) {
	$mm--;
}
if(($nn+1/2)*sqrt(3)*$pitch+$dy*$hexsize > $ys) {
	$nn--;
}
$XP=int($pitch*$cnvfactor+0.5);
$YP=int($pitch*$cnvfactor*sqrt(3)+0.5);
$XN=($mm+1)*2;
$YN=($nn+1)*2;
$xorig=-($mm+1/2)*$pitch;
$yorig=($nn+1/2)*sqrt(3)*$pitch;
print "RL 0,0,$XP,$YP,$XN,$YN\n";
&output_hex();
print "RE\n";

&output_VSTail;

close(OUT);
close(OUT2);

sub output_hex {
	local($x1,$x2,$x3,$x4,$x5,$x6,$y1,$y2,$y3,$y4,$y5,$y6);
	$x1 = $xorig + $hexsize*$rx1;
	$x2 = $xorig + $hexsize*$rx2;
	$x3 = $xorig + $hexsize*$rx3;
	$x4 = $xorig + $hexsize*$rx4;
	$x5 = $xorig + $hexsize*$rx5;
	$x6 = $xorig + $hexsize*$rx6;

	$y1 = $yorig + $hexsize*$ry1;
	$y2 = $yorig + $hexsize*$ry2;
	$y3 = $yorig + $hexsize*$ry3;
	$y4 = $yorig + $hexsize*$ry4;
	$y5 = $yorig + $hexsize*$ry5;
	$y6 = $yorig + $hexsize*$ry6;

	@xy=($x1,$y1,$x2,$y2,$x3,$y3,$x4,$y4,$x5,$y5,$x6,$y6);
	print OUT "poly($x1,$y1,$x2,$y2,$x3,$y3,$x4,$y4,$x5,$y5,$x6,$y6);\n";
	print OUT2 &autolisp::draw::draw_poly_autolisp(@xy),"\n";
	&Poly;
}

sub pattern_A {
	$a1x = 1;
	$a1y = 0;
	$a2x = 1/2;
	$a2y = sqrt(3)/2;
	$pitch/sqrt(3);
}

sub pattern_B {
	$a1x = sqrt(3)/2;
	$a1y = 1/2;
	$a2x = sqrt(3)/2;
	$a2y = -1/2;
	$pitch/2;
}

sub hex_pattern1 {
	$dx= 1;
	$rx1 = 1;
	$rx2 = 1/2;
	$rx3 = -1/2;
	$rx4 = -1;
	$rx5 = -1/2;
	$rx6 = 1/2;

	$dy= sqrt(3)/2;
	$ry1 = 0;
	$ry2 = sqrt(3)/2;
	$ry3 = sqrt(3)/2;
	$ry4 = 0;
	$ry5 = -sqrt(3)/2;
	$ry6 = -sqrt(3)/2;
}

sub hex_pattern2 {
	$dx= sqrt(3)/2;
	$rx1 = sqrt(3)/2;
	$rx2 = 0;
	$rx3 = -sqrt(3)/2;
	$rx4 = -sqrt(3)/2;
	$rx5 = 0;
	$rx6 = sqrt(3)/2;

	$dy= 1;
	$ry1 = 1/2;
	$ry2 = 1;
	$ry3 = 1/2;
	$ry4 = -1/2;
	$ry5 = -1;
	$ry6 = -1/2;
}

sub output_VSheader {
	print OUT "Procedure LoadFile;\n";
	print OUT "VAR\n";
	print OUT "  hatchName:STRING;\n";
	print OUT "result, index:INTEGER;\n";
	print OUT "boolResult:BOOLEAN;\n";
	print OUT "   tempHandle, tempHandle1, tempHandle2:HANDLE;\n\n";
	print OUT "BEGIN\n\n";
	print OUT "DrwSize(1,1);\n";
	print OUT "SetUnits(10000,1,0,25.4,'',' sq mc');\n";
	print OUT "PrimaryUnits(0,1,100,3,1,TRUE,FALSE);\n";
	print OUT "GridLines(50);\n";
	print OUT "PenGrid(5);\n";
	print OUT "DoubLines(0);\n";
	print OUT "SetScale(1);\n";
	print OUT "ClosePoly;\n";
}

sub output_VSTail {
	print OUT "END;\n\n";
	print OUT "Run(LoadFile);\n";
}

sub xconv {
	int(($_[0]+$xs)*$cnvfactor+0.5);
}

sub yconv{
	int(($ys-$_[0])*$cnvfactor+0.5);
}

##### from VStoJ01 ######
sub Poly{
#	@xy = split(/,/,$_[0]);
	@xpts = ();
	@ypts = ();
	@points = ();
	while(@xy)	{
		$xpt = xconv(shift(@xy));
		$ypt = yconv(shift(@xy));
		push(@xpts,$xpt);
		push(@ypts,$ypt);
		push(@points,"$xpt,$ypt");
	}
	$check = &check_clockwize;
	if($check == 0) {
		print "TK/0 ";
		print join('-',@points);
		print "\n";
	} elsif($check == 1) {
		print "TK/0 ";
		print join('-',reverse(@points));
		print "\n";
	} elsif($check == -1) {
		print "* cannot check TK rotation direction\n";
		print "*TK/0 ";
		print join('-',@points);
		print "\n*or\n*TK/0 ";
		print join('-',reverse(@points));
		print "\n";
	}
#	print STDOUT join('-',@points);
}

sub check_clockwize {
	local($angle);
	$angle = calculate_angle(($xpts[0]+$xpts[$#xpts])/2,($ypts[0]+$ypts[$#ypts])/2);
	if($angle > 0.0001) {
		0;
	} elsif($angle <-0.0001) {
		1;
	} else {
			-1;
	}
}

sub calculate_angle {
	local($x_g,$y_g)=@_;
	local($angle,$i,$vx1,$vy1,$vx2,$vy2);
	$angle = 0;
#	print "$x_g, $y_g\n";
	for($i=1;$i<=$#xpts;$i++) {
		$vx1 = $xpts[$i-1] - $x_g;
		$vy1 = $ypts[$i-1] - $y_g;
		$vx2 = $xpts[$i] - $x_g;
		$vy2 = $ypts[$i] - $y_g;
		$angle += atan2($vx1*$vy2-$vx2*$vy1,$vx1*$vx2+$vy1*$vy2);
#		print "($vx1,$vy1)";
 }
#	$vx1 = $xpts[$#xpts] - $x_g;
#	$vy1 = $ypts[$#ypts] - $y_g;
#	print "($vx1,$vy1)\n";
#	$vx2 = $xpts[0] - $x_g;
#	$vy2 = $ypts[0] - $y_g;
#	$angle += atan2($vx1*$vy2-$vx2*$vy1,$vx1*$vx2+$vy1*$vy2);
	$angle = $angle/3.141592;
#	print "TK $angle\n";
#	$angle;
}
