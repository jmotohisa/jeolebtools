#!/usr/local/perl

# J01toAL version 0.1a 2018/11/02
# Convert JEOL J01 format to autolisp
# commands supported:
#  RT, TK, LN
###
# revision history
# ver 0.1a 2018/11/02: initial version based on J01toVS
#

use lib './';
require 'autolisp.pl';

open(OUT,">al.lsp") || die "cannot open file\n";

foreach $aFile (@ARGV) {
#
	print "File Conversion: JEOL 01 format -> Autolisp\n";
	print "version 0.1a 2018/11/02 by J. Motohsia\n\n";
#
	print "J01 format file name: $aFile\n";
	print "conversion factor (default = 10) ? ";
	$cnvfactor= <STDIN>;
	chop $cnvfactor;
	if(length($cnvfactor)==0) {
		$cnvfactor = 10;
	}
	print "magnification (default = 1) ? ";
	$mag = <STDIN>;
	chop $mag;
	if(length($mag)==0) {
		$mag = 1;
	}
	$cnvfactor *= $mag;
	print "chip size shift x (default = 50)? ";
	$shiftx = <STDIN>;
	chop $shiftx;
    if(length($shiftx)==0) {
        $shiftx = 50;
    }
	print "chip size shift y (default = 50) ? ";
	$shifty = <STDIN>;
	chop $shifty;
    if(length($shifty)==0) {
        $shifty = 50;
    }
	print "limit RL (default = 10) ? ";
	$RLmax= <STDIN>;
	chop $RLmax;
	if(length($RLmax)==0) {
		$RLmax=10;
	}
#
	open(IN,$aFile);
	$lineno = 0;
	# print OUT "Procedure LoadFile;\n";
	# print OUT "VAR\n";
	# print OUT "  hatchName:STRING;\n";
	# print OUT "result, index:INTEGER;\n";
	# print OUT "boolResult:BOOLEAN;\n";
	# print OUT "   tempHandle, tempHandle1, tempHandle2:HANDLE;\n\n";
	# print OUT "BEGIN\n\n";
	# print OUT "DrwSize(2,2);\n";
	# print OUT "SetUnits(10000,1,0,25.4,'',' sq mc');\n";
	# print OUT "PrimaryUnits(0,1,100,3,1,TRUE,FALSE);\n";
	# print OUT "GridLines(50);\n";
	# print OUT "PenGrid(5);\n";
	# print OUT "DoubLines(0);\n";
	# print OUT "SetScale(5);\n";
	# print OUT "ClosePoly;\n";

	&conversion_main;

	# print OUT "END;\n\n";
	# print OUT "Run(LoadFile);\n";
	close(IN);
}
close(OUT);

# conversion routine version 0.1a
sub conversion_main	{
	while($line=<IN>) {
		$lineno++;
		$command = &get_commands;
# check RL/RE or others
		if ($command =~ /RL/) {
			case_RL($');
			@cmdstack0 = ();
			while($line=<IN>)		{
				$lineno++;
				$command = &get_commands;
				if ($command =~ /RE/) {
					last;
				} else {
					push(@cmdstack0,$command);
				}
			}
			for($ix = 0; $ix < $nxRL0; $ix++) {
				$xoff0 = $xRL0 + $xpRL0 * $ix;
				for($iy = 0; $iy < $nyRL0; $iy++) {
					$yoff0 = $yRL0 + $ypRL0 * $iy;
					if(($ix<$RLmax || $nxRL0-$ix<=$RLmax) && ($iy<$RLmax || $nyRL0-$iy<=$RLmax)) {
						parahrase_commandstack($xoff0,$yoff0,@cmdstack0);
					}
				}
			}
		} else {
			paraphrase_commands($command,0,0);
		}
	}
}

# subroutine to get command(s)
sub get_commands {
	local($line2);
	chop $line;
	@lines = ();
# skip comment line {...} and concatenate multiple lines for one commands
	if($line =~ /^\*/) {
		'NOP';
	} else {
		if($line !~ /\$$/) {
			$line;
		} else {
			push(@lines,$line);
			while($line=<IN>) {
				$lineno++;
				push(@lines,$line);
				if($line !~ /\$$/) {
					last;
				}
			}
			$line2 = join('',@lines);
#			print "$line2\n";
			$line2 =~ s/\$/\-/g;
#			print "$line2\n";
			$line2;
		}
	}
}

sub parahrase_commandstack {
	local($command,$xoffset,$yoffset);
	$xoffset = shift(@_);
	$yoffset = shift(@_);
	foreach $command (@_) {
		paraphrase_commands($command,$xoffset,$yoffset);
	}
}

# paraphrase JEOL01 commands
#  except for RL and RE
sub paraphrase_commands {
	local($command,$xoff,$yoff) = @_;
	
	if ($command=~ /RT\/([0-9])\s/) {
		&case_RT($',$xoff,$yoff);
	} elsif ($command =~ /TK\/([0-9])\s/) {
		&case_TK($',$xoff,$yoff);
	} elsif ($command =~ /LN\/([0-9])\s/) {
		&case_LN($',$xoff,$yoff);
	} elsif ($command =~ /RG\/([0-9])\s/) {
		&case_RG($',$xoff,$yoff);
	} elsif ($command =~ /ID\/([a-zA-Z0-9]*)/) {
		&case_ID($1);
	} elsif ($command =~ /EF/) {
		&case_EF;
	} elsif ($command =~ /CT/) {
		&case_CT;
	} elsif ($command =~ /NOP/) {
	}	else {
		print OUT ";;( SYNTAX ERROR in line $lineno )\n";
		print OUT ";;(  $command )\n";
	}
}

#
# conversion routine
#
sub xconv {
	($_[0]+$g_xoffset) /$cnvfactor - $shiftx;
}

sub yconv {
	$shifty - ($_[0]+$g_yoffset) /$cnvfactor;
}

#
# J01 format commands
#
sub case_ID {
	print OUT ";; (Layer(\'$_[0]\'));\n";

}

sub case_EF {
# do nothing
	print OUT ";; (EF)\n";
}

sub case_CT {
# do nothing
	print OUT ";; (CT)\n";
}

sub case_RT {
	@points = split(/;/,$_[0]);
	$g_xoffset = $_[1];
	$g_yoffset = $_[2];

	foreach $point (@points) {
		@spoints=split(/,/,$point);
		$x1 = $spoints[0];
		$y1 = $spoints[1];
		$x11 = $spoints[2];
		$y11 = $spoints[3];
		$xstart = xconv($x1);
		$ystart = yconv($y1);
		$xend = xconv($x1 + $x11);
		$yend = yconv($y1 + $y11);
		# print OUT "Rect($xstart,$ystart,$xend,$yend);\n";
		print OUT &autolisp::draw::draw_rectangle_autolisp($xstart,$ystart,$xend,$yend),"\n";
	}
}

sub case_TK {
	@polys = ();
	@polys = split(/;/,$_[0]);
	$g_xoffset = $_[1];
	$g_yoffset = $_[2];
	local(@output);

	foreach $poly (@polys) {
		print OUT ";;Poly(";
		@newpoints = ();
		@points = split(/-/,$poly);
		@endpoint = split(/,/,$points[0]);
		$xend = xconv($endpoint[0]);
		$yend = yconv($endpoint[1]);
		foreach $point (@points) {
			@spoints=split(/,/,$point);
			$x1 = xconv($spoints[0]);
			$y1 = yconv($spoints[1]);
			push(@newpoints,$x1);
			push(@newpoints,$y1);
		}
		$output = join(',',@newpoints);
		print OUT "$output);\n";
	}
	print OUT &autolisp::draw::draw_poly_autolisp(@newpoints),"\n";
}

sub case_LN {
	@points = split(/-/,$_[0]);
	$g_xoffset = $_[1];
	$g_yoffset = $_[2];

	foreach $point (@points) {
		@endpoint = split(/,/,$point);
		$x1 = xconv($endpoint[0]);
		$y1 = yconv($endpoint[1]);
		push(@newpoints,$x1);
		push(@newpoints,$y1);
	}
	print OUT &autolisp::draw::draw_line_autolisp(@newpoints),"\n";
}


sub case_RG {
	@points = split(/,/,$_[0]);
	$g_xoffset = $_[1];
	$g_yoffset = $_[2];

#	print OUT "{RG command is not supported !}\n";
	print OUT ";;(  RG/0 $_[0] )\n";

	$xc = $points[0];
	$yc = $points[1];
	$r2 = $points[3];
	$ai = $points[4];
	$af = $points[5];
	$x1 = xconv($xc-$r2);
	$y1 = yconv($yc+$r2);
	$x2 = xconv($xc+$r2);
	$y2 = yconv($yc-$r2);
	print OUT ";;Arc($x1,$y1,$x2,$y2,$ai,$af);\n"	
}

sub case_RL {
	@points = split(/,/,$_[0]);
	$xRL0 = shift(@points);
	$yRL0 = shift(@points);
	$xpRL0 = shift(@points);
	$ypRL0 = shift(@points);
	$nxRL0 = shift(@points);
	$nyRL0 = shift(@points);
}
