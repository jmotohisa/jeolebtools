#/usr/bin/perl

# jdfanalyzer_AL.pl ver 0.1a 2020/11/15
# a simple analyazer of JDF format file
###
# revision history
# ver 0.1a 2020/11/15: initial version based on jdfanalzyer.pl
#

use lib './';
require 'autolisp.pl';

open(OUT,">al.lsp") || die "cannot open file\n";

# Analyze Array in JDF file
foreach $aFile (@ARGV) {
#
    print "magnification (default = 10 for 100micron=10mm) ? ";
    $cnvfactor= <STDIN>;
    chop $cnvfactor;
    if(length($cnvfactor)==0) {
	$cnvfactor = 10;
    }
    print "default x-chip size (micron) ? ";
    $xsize = <STDIN>;
    print "default y-chip size (micron) ? ";
    $ysize = <STDIN>;
    #
    open(IN,$aFile);
    $lineno = 0;
    
    &init_colortable;
    &conversion_main;
    
    close(IN);
}
close(OUT);

sub conversion_main	{
    $layer = 1;
    while($line=<IN>) {
	$lineno++;
	$command = &get_commands;
	# serach for ARRAY commands
	if ($command =~ /ARRAY/) {
	    #  store commands into command stack until AEND
	    @cmdstack0 = $command;
	    while($line=<IN>)		{
		$lineno++;
		$command = &get_commands;
		if ($command =~ /AEND/) {
		    last;
		} else {
		    push(@cmdstack0,$command);
		}
	    }
	    &paraphrase_ARRAYcommands;
	    $layer++;
	} else {
	    #  NOP
	}
    }
}

sub get_commands {
    chop $line;
    # skip comment line ;
    if($line =~ /;/) {
	$`;
    } else {
	$line;
    }
}

sub paraphrase_ARRAYcommands {
    shift(@cmdstack0) =~ /ARRAY\s+\(([---0-9,\s]+)\)\/\(([---0-9,\s]+)\)/;
    $xarray = $1;
    $yarray = $2;
    @xa = split(/,/,$xarray);
    $x0 = $xa[0];
    $xn = $xa[1];
    $xstep = $xa[2];
    @ya = split(/,/,$yarray);
    $y0 = $ya[0];
    $yn = $ya[1];
    $ystep = $ya[2];
    
    $layer2 = $layer % 26;
    $fred = $red[$layer2];
    $fgrn = $grn[$layer2];
    $fblu = $blu[$layer2];
    
    print OUT ";; (Layer(\'$_[0]\'));\n";
    print OUT ";; (FillBack($fred,$fgrn,$fblu));\n";
    
    for($ix = 0; $ix < $xn; $ix++) {
	$xstart = ($x0 + $xstep * $ix - $xsize/2)/$cnvfactor;
	$xend = $xstart + $xsize/$cnvfactor;
	for($iy = 0; $iy < $yn; $iy++) {
	    $ystart = ($y0 - $ystep* $iy + $ysize/2)/$cnvfactor;
	    $yend = $ystart - $ysize/$cnvfactor;
	    print OUT &autolisp::draw::draw_rectangle_autolisp($xstart,$ystart,$xend,$yend),"\n";
	}
    }
}

sub init_colortable {
    @red=(32767,65535,0,32767,65535,0,32767,65535,
	  0,32767,65535,0,32767,65535,0,32767,65535,
	  0,32767,65535,0,32767,65535,0,32767,65535);
    @grn=(0,0,32767,32767,32767,65535,65535,65535,
	  0,0,0,32767,32767,32767,65535,65535,65535,
	  0,0,0,32767,32767,32767,65535,65535,65535);
    @blu=(0,0,0,0,0,0,0,0,
	  32767,32767,32767,32767,32767,32767,32767,32767,32767,
	  65535,65535,65535,65535,65535,65535,65535,65535,65535);
}
