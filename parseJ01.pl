package J01::parse;

use strict;

#
# J01 format commands
#

## ID
sub case_ID {
	"Layer(\'$_[0]\');\n";
}

## EF
sub case_EF {
# do nothing
	"{ EF }\n";
}

sub case_CT {
# do nothing
	"{ CT }\n";
}

sub case_RT {
	my @points = split(/;/,$_[0]);
	my $g_xoffset = $_[1];
	my $g_yoffset = $_[2];
	my @rects=();
	my @rect=();

	foreach my $point (@points) {
		my @spoints=split(/,/,$point);
		my $x1 = $spoints[0];
		my $y1 = $spoints[1];
		my $x11 = $spoints[2];
		my $y11 = $spoints[3];
		# my $xstart = xconv($x1);
		# my $ystart = yconv($y1);
		# my $xend = xconv($x1 + $x11);
		# my $yend = yconv($y1 + $y11);
		@rect=($x1,$y1,$x1+$x11,$y1+$y11);
		push(@rects,@rect);
		# print OUT "Rect($xstart,$ystart,$xend,$yend);\n";
	}
	@rects;
}

sub case_TK {
	my @polys0 = split(/;/,$_[0]);
	# my $g_xoffset = $_[1];
	# my $g_yoffset = $_[2];
	my @polys=();

	foreach my $a_poly (@polys0) {
		my @newpoints = ();
		my @points = split(/-/,$a_poly);
		my @endpoint = split(/,/,$points[0]);
		# $xend = xconv($endpoint[0]);
		# $yend = yconv($endpoint[1]);
		my @poly=();
		foreach my $point (@points) {
			push (@poly,($endpoint[0],$endpoint[1]));
		}
		push(@polys,@poly);
	}
	@polys;
}

sub case_LN {
	my @points = split(/-/,$_[0]);
	my @lines=();

	my @spoints=split(/,/,shift(@points));
	while(my $point = shift(@points)) {
		my @spoints=split(/,/,$point);
		push(@lines,($spoints[0],$spoints[1]));
	}
	@lines;
}

sub case_RG {
	my @points = split(/,/,$_[0]);
	my @arc=();
	
	my $xc = $points[0];
	my $yc = $points[1];
	my $r2 = $points[3];
	my $ai = $points[4];
	my $af = $points[5];
	my $x1 = xconv($xc-$r2);
	my $y1 = yconv($yc+$r2);
	my $x2 = xconv($xc+$r2);
	my $y2 = yconv($yc-$r2);

	@arc=($x1,$y1,$x2,$y2,$ai,$a);
}

# RL: repeat start
# return list of (xRL,yRL,xpRL,ypRL,nxRL,nyRL)
sub case_RL {
	my $line=$_[0];
	my @points = split(/,/,$line);
	@points;
	# my $xRL0 = shift(@points);
	# my $yRL0 = shift(@points);
	# my $xpRL0 = shift(@points);
	# my $ypRL0 = shift(@points);
	# my $nxRL0 = shift(@points);
	# my $nyRL0 = shift(@points);
}

# conversion xconv($x,$g_xoffset,$shiftx,$cnvfactor)
sub xconv {
	my $x=$_[0];
	my $g_xoffset=$_[1];
	my $shiftx=$_[2];
	my $cnvfactor=$_[3];
	($x+$g_xoffset) /$cnvfactor - $shiftx;
}

# conversion yconv($y,$g_yoffset,$shifty,$cnvfactor)
sub yconv {
	my $y=$_[0];
	my $g_yoffset=$_[1];
	my $shifty=$_[2];
	my $cnvfactor=$_[3];
	$shifty - ($y+$g_yoffset) /$cnvfactor;
}
