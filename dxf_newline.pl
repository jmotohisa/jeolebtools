print >> EOF
	0
	"SECTION"
	2
	"ENTITIES"
	0
EOF

print >> EOF2
	0
	"ENDSEC"
	0
	"EOF"
EOF2


sub DXF_newline {
	local(@pts)=@_;
	my @data;
	$data[0]="0";
	$data[1]="LINE";
	$data[2]="8";
	$data[3]="_0-0_";
	$data[4]="7";
	$data[5]="CONTINUOUS";
	$data[6]="62";
	$data[7]="7";
	$data[8]="10";
	$data[9]=$pts[0];
	$data[10]="20";
	$data[11]=$pts[1];
	$data[12]="11";
	$data[13]=$pts[2];
	$data[14]="21";
	$data[15]=$pts[3];

	@data;
}
