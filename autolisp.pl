package autolisp::draw;

sub draw_rectangle_autolisp {
	local($x1,$y1,$x2,$y2)=@_;
	"(command \"._rectangle\" \"$x1,$y1\" \"$x2,$y2\")";
}

sub draw_circle_autolisp {
	local ($x,$y,$radius)=@_;
	"(command \"._circle\" \"$x,$y\" \"$radius\")";
}

sub draw_line_autolisp {
	local (@pts)=@_;
	local (@command,$cmd0,$x,$y);
	push(@command,"(command \"._line\"");
	while(@pts) {
		$x=shift(@pts);
		$y=shift(@pts);
		$cmd0="\"$x,$y\"";
		push(@command,$cmd0);
	}	
	push(@command, "\"\")");
	join(' ',@command);
}

sub draw_poly_autolisp {
	local (@pts)=@_;
	local (@command,$cmd0,$x,$y);
	push(@command,"(command \"._pline\"");
	while(@pts) {
		$x=shift(@pts);
		$y=shift(@pts);
		$cmd0="\"$x,$y\"";
		push(@command,$cmd0);
	}	
	push(@command, "\"C\")");
	join(' ',@command);
}

sub draw_poly_autolisp {
	local (@pts)=@_;
	local (@command,$cmd0,$x,$y);
	push(@command,"(command \"._pline\"");
	while(@pts) {
		$x=shift(@pts);
		$y=shift(@pts);
		$cmd0="\"$x,$y\"";
		push(@command,$cmd0);
	}
	push(@command, "\"C\")");
	join(' ',@command);
}
# (command "_.pline"
# 	(foreach pt lst (command "_non" pt))

1;
