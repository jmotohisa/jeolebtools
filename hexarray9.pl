#!/usr/local/bin/jperl
# create pattern for triangular lattice array of hexagon
# create Vectorwoks Script data
#

print "Hexagonal Array Maker ver9.0\n";
print "all rights reserved by Motohisa&Noborisaka\n\n";
print "!!!!!!!!!!!!!NOTICE!!!!!!!!!!!!!!!\n";
print "modify the definition of diameter:d\nd:distance between facing edge\n\n\n";
print STDOUT "Save as filename?(*.j01 , *.vs)? : ";
$filename = <STDIN>;
chop $filename;


$true=1;


open(OUT,">$filename.j01") || die "cannot open file\n";

open(VSOUT,">$filename.vs") || die "cannot open file\n";

&output_VSheader;

$count=1;

while($true==1) {

# assume latticetype is A (normal lattice)
$latticeType=0;
#print "Type of lattice (A:0(default) or B:1) ? :";
#$latticeType = <STDIN>;
#chop $latticeType;

print "pitch of the lattice (micron) ? :";
$pitch = <STDIN>;
chop $pitch;
while(length($pitch)==0) {
print "pitch of the lattice (micron) ? :";
$pitch = <STDIN>;
chop $pitch;
}
print OUT"***pitch=$pitch,";

$latticeType=1;

if($latticeType ==0) {
	$max_hsize = &pattern_A;
} else {
	$max_hsize = &pattern_B;
}

print "size of the hexagon (micron) ? :";
$hexsize = <STDIN>;
chop $hexsize;
while(length($hexsize)==0) {
print "size of the hexagon (micron) ? :";
$hexsize = <STDIN>;
chop $hexsize;
}

while ($hexsize>=$max_hsize+$max_hsize) {
print "size of the hexagon is too large!\n"; 
print "size of the hexagon (micron) ? :";
$hexsize = <STDIN>;
chop $hexsize;
}
print OUT"size=$hexsize***\n";


print "ID name ? : ";
$ID = <STDIN>;
chop $ID;
while(length($ID)==0) {
print "ID name ? : ";
$ID = <STDIN>;
chop $ID;
}

print OUT "ID/$ID\n";

print "Size of the whole pattern (x in micron(default =100)) ? : ";
$sizex = <STDIN>;
chop $sizex;
if(length($sizex)==0) {
	$sizex = 100;
}

print "Size of the whole pattern (y in micron(default =100)) ? : ";
$sizey = <STDIN>;
chop $sizey;
if(length($sizey)==0) {
	$sizey = 100;
}

print "conversion factor (default =10) ? ";
	$cnvfactor= <STDIN>;
	chop $cnvfactor;
if(length($cnvfactor)==0) {
	$cnvfactor = 10;
}
print "magnification (default = 250) ? ";
	$mag = <STDIN>;
	chop $mag;
	if(length($mag)==0) {
		$mag = 250;
	}
	$cnvfactor *= $mag;

&hex_pattern2;

$xs = $sizex/2;
$ys = $sizey/2;
print VSOUT "Rect(-$xs,-$ys,$xs,$ys);\n";

$mm=int($xs/$pitch);
$nn=int($ys/($pitch*sqrt(3)));
if($mm*$pitch+$dx*$hexsize > $xs) {
	$mm--;
}
if($nn*$pitch*sqrt(3)+$dy*$hexsize > $ys) {
	$nn--;
}
$XP=int($pitch*$cnvfactor+0.5);
$YP=int($pitch*$cnvfactor*sqrt(3)+0.5);
$XN=$mm*2+1;
$YN=$nn*2+1;
$xorig=-$mm*$pitch;
$yorig=$nn*$pitch*sqrt(3);
print "RL 0,0,$XP,$YP,$XN,$YN\n";
print OUT "RL 0,0,$XP,$YP,$XN,$YN\n";
&output_hex();
print "RE\n";
print OUT "RE\n";

$mm=int(($xs-$pitch/2)/$pitch);
$nn=int(($ys-$pitch*sqrt(3)/2)/($pitch*sqrt(3)));
if(($mm+1/2)*$pitch+$dx*$hexsize > $xs) {
	$mm--;
}
if(($nn+1/2)*sqrt(3)*$pitch+$dy*$hexsize > $ys) {
	$nn--;
}
$XP=int($pitch*$cnvfactor+0.5);
$YP=int($pitch*$cnvfactor*sqrt(3)+0.5);
$XN=($mm+1)*2;
$YN=($nn+1)*2;
$xorig=-($mm+1/2)*$pitch;
$yorig=($nn+1/2)*sqrt(3)*$pitch;
print "RL 0,0,$XP,$YP,$XN,$YN\n";
print OUT "RL 0,0,$XP,$YP,$XN,$YN\n";
&output_hex();
print "RE\n";
print OUT "RE\n";

print OUT "EF\n";

&output_VSTail;

print STDOUT "continue (y/n) ? ";
	$res = <STDIN>;
	chop $res;
if($res eq 'y') {$true=1;} elsif($res eq 'n') {$true=0;}
until($true==1 or $true==0) {
print STDOUT "continue (y/n) ? ";
	$res = <STDIN>;
	chop $res;
}
if($true==1) {print "next step\n";}
if($true==0) {last;}
}

$count=$count+1;

close(VSOUT);
close(OUT);

sub output_hex {
	$x1 = $xorig + $hexsize*$rx1;
	$x2 = $xorig + $hexsize*$rx2;
	$x3 = $xorig + $hexsize*$rx3;
	$x4 = $xorig + $hexsize*$rx4;
	$x5 = $xorig + $hexsize*$rx5;
	$x6 = $xorig + $hexsize*$rx6;

	$y1 = $yorig + $hexsize*$ry1;
	$y2 = $yorig + $hexsize*$ry2;
	$y3 = $yorig + $hexsize*$ry3;
	$y4 = $yorig + $hexsize*$ry4;
	$y5 = $yorig + $hexsize*$ry5;
	$y6 = $yorig + $hexsize*$ry6;

	@xy=($x1,$y1,$x2,$y2,$x3,$y3,$x4,$y4,$x5,$y5,$x6,$y6);
	print VSOUT "poly($x1,$y1,$x2,$y2,$x3,$y3,$x4,$y4,$x5,$y5,$x6,$y6);\n";
	&Poly;
}

sub pattern_A {
	$a1x = 1;
	$a1y = 0;
	$a2x = 1/2;
	$a2y = sqrt(3)/2;
	$pitch/sqrt(3);
}

sub pattern_B {
	$a1x = sqrt(3)/2;
	$a1y = 1/2;
	$a2x = sqrt(3)/2;
	$a2y = -1/2;
	$pitch/2;
}

sub hex_pattern1 {
	$dx= 1;
	$rx1 = 1/sqrt(3);
	$rx2 = 1/(2*sqrt(3));
	$rx3 = -1/(2*sqrt(3));
	$rx4 = -1/sqrt(3);
	$rx5 = -1/(2*sqrt(3));
	$rx6 = 1/(2*sqrt(3));

	$dy= sqrt(3)/2;
	$ry1 = 0;
	$ry2 = 1/2;
	$ry3 = 1/2;
	$ry4 = 0;
	$ry5 = -1/2;
	$ry6 = -1/2;
}

sub hex_pattern2 {
	$dx= 1/2;
	$rx1 = 1/2;
	$rx2 = 0;
	$rx3 = -1/2;
	$rx4 = -1/2;
	$rx5 = 0;
	$rx6 = 1/2;

	$dy= 1;
	$ry1 = 1/(2*sqrt(3));
	$ry2 = 1/sqrt(3);
	$ry3 = 1/(2*sqrt(3));
	$ry4 = -1/(2*sqrt(3));
	$ry5 = -1/sqrt(3);
	$ry6 = -1/(2*sqrt(3));
}

sub output_VSheader {
	print VSOUT "Procedure LoadFile;\n";
	print VSOUT "VAR\n";
	print VSOUT "  hatchName:STRING;\n";
	print VSOUT "result, index:INTEGER;\n";
	print VSOUT "boolResult:BOOLEAN;\n";
	print VSOUT "   tempHandle, tempHandle1, tempHandle2:HANDLE;\n\n";
	print VSOUT "BEGIN\n\n";
	print VSOUT "DrwSize(1,1);\n";
	print VSOUT "SetUnits(10000,1,0,25.4,'',' sq mc');\n";
	print VSOUT "PrimaryUnits(0,1,100,3,1,TRUE,FALSE);\n";
	print VSOUT "GridLines(50);\n";
	print VSOUT "PenGrid(5);\n";
	print VSOUT "DoubLines(0);\n";
	print VSOUT "SetScale(1);\n";
	print VSOUT "ClosePoly;\n";
}

sub output_VSTail {
	print VSOUT "END;\n\n";
	print VSOUT "Run(LoadFile);\n";
}

sub xconv {
	int(($_[0]+$xs)*$cnvfactor+0.5);
}

sub yconv{
	int(($ys-$_[0])*$cnvfactor+0.5);
}

##### from VStoJ01 ######
sub Poly{
#	@xy = split(/,/,$_[0]);
	@xpts = ();
	@ypts = ();
	@points = ();
	while(@xy)	{
		$xpt = xconv(shift(@xy));
		$ypt = yconv(shift(@xy));
		push(@xpts,$xpt);
		push(@ypts,$ypt);
		push(@points,"$xpt,$ypt");
	}
	$check = &check_clockwize;
	if($check == 0) {
		print "TK/0 ";
              print OUT "TK/0 ";
		print join('-',@points);
              print OUT join('-',@points);
              print "\n";
              print OUT "\n";
	} elsif($check == 1) {
		print "TK/0 ";
              print OUT "TK/0 ";
		print join('-',reverse(@points));
              print OUT join('-',reverse(@points));
		print "\n";
              print OUT "\n";
	} elsif($check == -1) {
		print "* cannot check TK rotation direction\n";
              print OUT "* cannot check TK rotation direction\n";
		print "*TK/0 ";
              print OUT "*TK/0 ";
		print join('-',@points);
              print OUT join('-',@points);
		print "\n*or\n*TK/0 ";
              print OUT "\n*or\n*TK/0 ";
		print join('-',reverse(@points));
              print OUT join('-',reverse(@points));
		print "\n";
              print OUT "\n";
	}
#	print STDOUT join('-',@points);
}

sub check_clockwize {
	local($angle);
	$angle = calculate_angle(($xpts[0]+$xpts[$#xpts])/2,($ypts[0]+$ypts[$#ypts])/2);
	if($angle > 0.0001) {
		0;
	} elsif($angle <-0.0001) {
		1;
	} else {
			-1;
	}
}

sub calculate_angle {
	local($x_g,$y_g)=@_;
	local($angle,$i,$vx1,$vy1,$vx2,$vy2);
	$angle = 0;
#	print "$x_g, $y_g\n";
	for($i=1;$i<=$#xpts;$i++) {
		$vx1 = $xpts[$i-1] - $x_g;
		$vy1 = $ypts[$i-1] - $y_g;
		$vx2 = $xpts[$i] - $x_g;
		$vy2 = $ypts[$i] - $y_g;
		$angle += atan2($vx1*$vy2-$vx2*$vy1,$vx1*$vx2+$vy1*$vy2);
#		print "($vx1,$vy1)";
 }
#	$vx1 = $xpts[$#xpts] - $x_g;
#	$vy1 = $ypts[$#ypts] - $y_g;
#	print "($vx1,$vy1)\n";
#	$vx2 = $xpts[0] - $x_g;
#	$vy2 = $ypts[0] - $y_g;
#	$angle += atan2($vx1*$vy2-$vx2*$vy1,$vx1*$vx2+$vy1*$vy2);
	$angle = $angle/3.141592;
#	print "TK $angle\n";
#	$angle;
}

