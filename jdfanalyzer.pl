#!/usr/bin/perl
# jdfanalyzer ver 0.02a
#
use strict;
use warnings;
use utf8;
use Getopt::Long qw(:config posix_default no_ignore_case gnu_compat);

use constant LF => "\n";

our @red;
our @grn;
our @blu;
my $cnvfactor=10;
my $xsize=100;
my $ysize=100;
my $help = 0;

my $message = <<EOT;
# Usage: jdfanalyzer.pl [-c <conversion factor>] [-x <xsize>] [-y <ysize>] jdffile [jdffile...]
#   -c --cnvfactor :conversion factor (defailt $cnvfactor)
#   -x --xsize : chip size in the x direction (default $xsize um)
#   -y --ysize : chip size in the y direction (default $ysize um)
#   -h --help  : display this message
EOT
    
GetOptions(
    'cnvfactor|c=i' => \$cnvfactor,
    'xsize|x' => \$xsize,
    'ysize|y' => \$ysize,
    'help|h' => \$help
    );

if ($help == 1) {
    print STDERR $message.LF;
    exit(0);
}
	

# open(OUT,">vs.txt") || die "cannot open file\n";
# Analyze Array in JDF file
foreach my $aFile (@ARGV) {
    #
    ## old implementation
    # print "magnification (default = 10 for 100micron=10mm) ? ";
    # my $cnvfactor= <STDIN>;
    # chop $cnvfactor;
    # if(length($cnvfactor)==0) {
    # 	$cnvfactor = 10;
    # }
    # print "default x-chip size (micron) ? ";
    # my $xsize = <STDIN>;
    # chomp $xsize;
    # print "default y-chip size (micron) ? ";
    # my $ysize = <STDIN>;
    # chomp $ysize;
    #
    my $fname_out;
    $fname_out=$aFile;
    $fname_out =~ s/\.jdf$//;
    $fname_out =~ s/\.JDF$//;
    $fname_out =$fname_out.".txt";
    print "$cnvfactor,$xsize,$ysize: $fname_out\n";
    open(OUT,">$fname_out") || die "cannot open file\n";
    open(IN,$aFile);
    my $lineno = 0;
    print OUT "Procedure LoadFile;\n";
    print OUT "VAR\n";
    print OUT "  hatchName:STRING;\n";
    print OUT "result, index:INTEGER;\n";
    print OUT "boolResult:BOOLEAN;\n";
    print OUT "   tempHandle, tempHandle1, tempHandle2:HANDLE;\n\n";
    print OUT "BEGIN\n\n";
    print OUT "DrwSize(1,1);\n";
    print OUT "SetUnits(100000,1,0,25.4,'',' sq mm');\n";
    print OUT "PrimaryUnits(0,1,100,3,1,TRUE,FALSE);\n";
    print OUT "GridLines(10);\n";
    print OUT "PenGrid(1);\n";
    print OUT "DoubLines(0);\n";
    print OUT "SetScale(5);\n";
    print OUT "ClosePoly;\n";
    
    &init_colortable;
    &conversion_main(*IN,$xsize,$ysize,$cnvfactor);
    
    print OUT "END;\n\n";
    print OUT "Run(LoadFile);\n";
    close(IN);
    close(OUT);
}

sub conversion_main {
    local *IN = $_[0];
    my $xsize = $_[1];
    my $ysize = $_[2];
    my $cnvfactor = $_[3];
    my $layer = 1;
    while(my $line=<IN>) {
	my $lineno++;
	my $command = &get_commands($line);
	# serach for ARRAY commands
	if ($command =~ /ARRAY/i) {
	    #  store commands into command stack until AEND
	    my @cmdstack0 = $command;
	    while($line=<IN>)		{
		$lineno++;
		$command = &get_commands($line);
		if ($command =~ /AEND/i) {
		    last;
		} else {
		    push(@cmdstack0,$command);
		}
	    }
	    &paraphrase_ARRAYcommands($layer,$xsize,$ysize,$cnvfactor,\@cmdstack0);
	    $layer++;
	} else {
	    #  NOP
	}
    }
}

sub get_commands {
    my ($line) = @_;
    chop $line;
    # skip comment line ;
    if($line =~ /;/) {
	$`;
    } else {
	$line;
    }
}

sub paraphrase_ARRAYcommands {
    my ($layer,$xsize,$ysize,$cnvfactor,$cmdstack0) = @_;
    shift(@$cmdstack0) =~ /ARRAY/i;
    my $array = $'; # print "$array\n";
    $array =~ /\(([-0-9]+),([-0-9]+),([-0-9]+)\)/;
    my $x0=$1;
    my $xn=$2;
    my $xstep=$3;
    $array = $'; # print "$array\n";
    $array =~ /\(([-0-9]+),([-0-9]+),([-0-9]+)\)/;
    my $y0=$1;
    my $yn=$2;
    my $ystep=$3;
    # print "($x0,$xn,$xstep)\n";
    # print "($y0,$yn,$ystep)\n";

    # find skip
    my @xskip=();
    my @yskip=();
    foreach my $line (@$cmdstack0) {
	if ($line =~ /SKIP/i) {
	    my $skips = $';
	    $skips =~ /\((\d+),(\d+)\)/;
	    # print "$1,$2\n";
	    push(@xskip,$1);
	    push(@yskip,$2);
	}
    }
    # print "@xskip\n";
    # print "@yskip\n";
   
    my $layer2 = $layer % 26;
    my $fred = $red[$layer2];
    my $fgrn = $grn[$layer2];
    my $fblu = $blu[$layer2];

    # print "\n=====\n";
    print OUT "Layer(\'Layer$layer"."_"."$xsize"."x"."$ysize\');\n";
    print OUT "FillPat(1);\n";
    print OUT "FillBack($fred,$fgrn,$fblu);\n";
    print OUT "NameClass('pattern$layer');\n";
   
    for(my $ix = 0; $ix < $xn; $ix++) {
	my $xstart = ($x0 + $xstep * $ix - $xsize/2)/$cnvfactor;
	my $xend = $xstart + $xsize/$cnvfactor;
	for(my $iy = 0; $iy < $yn; $iy++) {
	    my $ystart = ($y0 - $ystep* $iy + $ysize/2)/$cnvfactor;
	    my $yend = $ystart - $ysize/$cnvfactor;
	    #	    $nskips=$#xskip;
	    if (&check_skip($ix+1,$iy+1,\@xskip,\@yskip)==0) {
		print OUT "Rect($xstart,$ystart,$xend,$yend);\n";
	    }
	}
    }

    my $x;
    my $y;
    my $ix;
    my $iy;
    
    foreach my $line (@$cmdstack0) {
	if($line =~ /ASSIGN/i) {
	    my $aline = $';
	    $aline =~ /\-\>/;
	    my $name = $`;
	    my $num = $';
	    if( $num =~ /\(\(([\d\s\*]+),([\d\s\*]+)\),(\w+)\)/) {
		$ix = $1;
		$iy = $2;
		my $shot = $3;
		unless ($ix eq '*' || $iy eq '*') {
		    $x = ($x0 + $xstep * ($ix-1) - $xsize/2)/$cnvfactor;
		    $y = ($y0 - $ystep * ($iy-1) + $ysize/2)/$cnvfactor;
		    # print "$x,$y,$name,$shot\n";
		    print OUT "NameClass('text$layer');\n";
		    &output_text($x,$y,$name."\n".$shot);
		}
		if($ix eq '*' && $iy eq '*') {
		    for($ix = 0; $ix < $xn; $ix++) {
			$x = ($x0 + $xstep * $ix - $xsize/2)/$cnvfactor;
			for($iy = 0; $iy < $yn; $iy++) {
			    $y = ($y0 - $ystep* $iy + $ysize/2)/$cnvfactor;
			    if (&check_skip($ix+1,$iy+1,\@xskip,\@yskip)==0) {
				$x = ($x0 + $xstep * ($ix) - $xsize/2)/$cnvfactor;
				$y = ($y0 - $ystep * ($iy) + $ysize/2)/$cnvfactor;
#				print $ix+1,",",$iy+1,",$name,$shot\n";
				# print "$x,$y,$name,$shot\n";
				print OUT "NameClass('text$layer');\n";
				&output_text($x,$y,$name."_".$shot);
			    }
			}
		    }
		}
	    } else {
		print "not matched :$num\n";
	    }
	}
	    
    }

}

sub init_colortable {
    @red=(32767,65535,0,32767,65535,0,32767,65535,
	  0,32767,65535,0,32767,65535,0,32767,65535,
	  0,32767,65535,0,32767,65535,0,32767,65535);
    @grn=(0,0,32767,32767,32767,65535,65535,65535,
	  0,0,0,32767,32767,32767,65535,65535,65535,
	  0,0,0,32767,32767,32767,65535,65535,65535);
    @blu=(0,0,0,0,0,0,0,0,
	  32767,32767,32767,32767,32767,32767,32767,32767,32767,
	  65535,65535,65535,65535,65535,65535,65535,65535,65535);
}

sub check_skip {
    my($ix,$iy,$xskip,$yskip)=@_;

    if($#$xskip < 0 || $#$yskip <0) {
	return 0;
    }
    
    # print "xskip=@$xskip,\nyskip = @$yskip\n";
    # print "$ix,$iy\n";
    
    my $flag_skip = 0;
    my @xresult = grep { $$xskip[$_] == $ix } 0 .. $#$xskip;
    # print "@$yskip\n";
    if($#xresult >= 0) {
	# print "(ix,iy)=($ix,$iy): @xresult,\n";
	foreach my $xres (@xresult) {
	    # print "$xres,$iy,$$yskip[$xres]\n";
	    if ($$yskip[$xres]==$iy) {
		return 1;
	    }
	}
    }
    return 0;
}

sub output_text {
    my ($x,$y,$text) = @_;

    print OUT "FillPat(0);\n";
    print OUT "FillFore(0,0,0);\n";
    print OUT "FillBack(65535,65535,65535);\n";
    print OUT "PenFore(0,0,0);\n";
    print OUT "PenBack(65535,65535,65535);\n";
    print OUT "TextFont(GetFontID('Osaka'));\n";
    print OUT "TextSize(12);\n";
    print OUT "TextFace([]);\n";
    print OUT "TextFlip(0);\n";
    print OUT "TextRotate(0);\n";
    print OUT "TextSpace(2);\n";
    print OUT "TextJust(2);\n";
    print OUT "TextVerticalAlign(3);\n";
    print OUT "TextOrigin($x,$y);\n";
    print OUT "BeginText;\n";
    print OUT "'$text'\n";
    print OUT "EndText;\n";
}
