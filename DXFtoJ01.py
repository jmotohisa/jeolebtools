# -*= coding: utf-8 -*-

import sys;
import dxfgrabber as dg
import argparse
import math

def get_args():
        parser = argparse.ArgumentParser(description='Convert DXF to J01 format.')

        if sys.stdin.isatty():
                parser.add_argument('fname', help='DXF file name', type=str)
                parser.add_argument('-x', '--xshift',\
                                    nargs='?',\
                                    type=float,\
                                    default = 50,\
                                    help='x-shift (default 50)')
                parser.add_argument('-y', '--yshift',\
                                    nargs='?',\
                                    type=float,\
                                    default = 50,\
                                    help='x-shift (default 50)')
                parser.add_argument('-c', '--cnvfactor',\
                                    nargs='?',\
                                    type=float,\
                                    default = 10,\
                                    help='conversion factor (default 10)')
                args = parser.parse_args()
                return(args)

                                        
# fname=sys.argv[1]
args = get_args()
if hasattr(args,'fname'):
        fname=args.fname
shiftx=args.xshift
shifty=args.yshift
cnvfactor=args.cnvfactor

dxf=dg.readfile(fname)

def J01_line(l):
        x0=xconv_to_J01(l.start[0],shiftx,cnvfactor)
        y0=yconv_to_J01(l.start[1],shifty,cnvfactor)
        x1=xconv_to_J01(l.end[0],shiftx,cnvfactor)
        y1=yconv_to_J01(l.end[1],shifty,cnvfactor)
        sys.stdout.write('LN/0 {},{}-{},{}'.format(x0,y0,x1,y1))

def J01_polyline(p):
        xpts=[None]*len(p.points)
        ypts=[None]*len(p.points)
        for i,point in enumerate(p.points):
                xpts[i]=point[0]
                ypts[i]=point[1]

        if p.is_closed:
                sys.stdout.write('TK/0 ')
                check=check_clockwize(xpts,ypts)
        else:
                sys.stdout.write('LN/0 ')
                check=0

        if check==0:
                start=0
                for point in p.points:
                        if start !=0:
                                sys.stdout.write('-')
                        start=1
                        x0=xconv_to_J01(point[0],shiftx,cnvfactor)
                        y0=yconv_to_J01(point[1],shifty,cnvfactor)
                        sys.stdout.write('{},{}'.format(x0,y0))
                sys.stdout.write('\n')
        elif check==1:
                start=0
                for point in reversed(p.points):
                        if start !=0:
                                sys.stdout.write('-')
                        start=1
                        x0=xconv_to_J01(point[0],shiftx,cnvfactor)
                        y0=yconv_to_J01(point[1],shifty,cnvfactor)
                        sys.stdout.write('{},{}'.format(x0,y0))
                sys.stdout.write('\n')
        else:
                print('* cannot check TK/rotation direction')
                start=0
                for point in p.points:
                        if start !=0:
                                sys.stdout.write('-')
                        start=1
                        x0=xconv_to_J01(point[0],shiftx,cnvfactor)
                        y0=yconv_to_J01(point[1],shifty,cnvfactor)
                        sys.stdout.write('{},{}'.format(x0,y0))
                sys.stdout.write('\n')
                

def check_clockwize(xpts,ypts):
        n=len(xpts)-1
        x0=xpts[0]
        y0=ypts[0]
        xn=xpts[n]
        yn=ypts[n]
        angle=calculate_angle((x0+xn)/2,(y0+yn)/2,xpts,ypts)
        if angle > 0.0001:
		return 0
	elif angle <-0.0001:
		return 1
	else :
		return -1
        
        # for xpt, ypt in zip(xpts, ypts):                     
        
def calculate_angle (x0,y0,xpts,ypts):
        angle = 0
        for i in range(0, len(xpts)-1):
                vx1 = xpts[i] - x0;
		vy1 = ypts[i] - y0;
		vx2 = xpts[i+1] - x0;
		vy2 = ypts[i+1] - y0;
		angle += math.atan2(vx1*vy2-vx2*vy1,vx1*vx2+vy1*vy2)
                
	angle = angle/3.141592
        return angle

def xconv_to_J01(x,shiftx,cnvfactor):
        return int((x+shiftx)+cnvfactor+0.5)

def yconv_to_J01(y,shifty,cnvfactor):
        return int((shifty-y)*cnvfactor+0.5)

for entity in dxf.entities:
    if entity.dxftype == 'LINE':
            J01_line(entity)
    elif entity.dxftype == 'POLYLINE':
            J01_polyline(entity)
    elif entity.dxftype == 'LWPOLYLINE':
            J01_polyline(entity)
    # elif entity.dxftype == 'CIRCLE':
    #         J01_circle(entity)
    # elif entity.dxftype == 'ARC':
    #         J01_arc(entity)
    else:
	    print('* ',entity.dxftype)
