#!/usr/bin/perl

# DXFtoJ01 version 0.1a 2018/11/05
# Convert DXF to JEOL J01 format
# commands supported:
#  RT, TK, LN
###
# revision history
# ver 0.1a 2018/11/05: initial version


use strict;
use warnings;

use Getopt::Long qw/:config posix_default no_ignore_case bundling auto_help/;
use Pod::Usage qw/pod2usage/;

my %opts = ( x => 50, y => 50, cnvfactor => 10, mag => 1, dialogue => "");

my $slient;
GetOptions(\%opts, qw(x=f y=f mag=f cnvfactor=f dialogue)) || die pod2usage(1);

my $shiftx=$opts{x};
my $shifty=$opts{y};
my $cnvfactor=$opts{cnvfactor};
my $mag=$opts{mag};
my $dialogue = $opts{dialogue};

open(OUT,"pattern.j01") || die "cannot open file\n";

foreach my $aFile (@ARGV) {
	if($dialogue) {
		print "File Conversion: VectorSciprt -> JEOL 01 format\n";
		print "version 0.1c 2003/1/23 by J. Motohsia\n\n";
		#
		print "VectorScript file name: $aFile\n";
		print "conversion factor (default =10) ? ";
		$cnvfactor= <STDIN>;
		chomp $cnvfactor;
		if(length($cnvfactor)==0) {
			$cnvfactor = 10;
		}
		print "magnification (default = 1) ? ";
		my $mag = <STDIN>;
		chomp $mag;
		if(length($mag)==0) {
			$mag = 1;
		}
		$cnvfactor *= $mag;
		# print "$mag,$cnvfactor\n";
		print "chip size shift x ? ";
		my $shiftx = <STDIN>;
		chomp $shiftx;
		print "chip size shift y ? ";
		my $shifty = <STDIN>;
		chomp $shifty;
	}
#
	open(IN,"$aFile");
	my $lineno=0;
	my $sectionFound = 0;
	while(my $line=<IN>) {
		$lineno++;
		if($line ~= "ENTITIES") {
			$sectionFound=1;
		}
		if($line ~= "ENDSEC" && $sectionFound==1) {
			$sectdionFound=0;
		}
		# &get_commands;
		# &paraphrase_commands;
	}
	close(IN);
}

close(OUT);
