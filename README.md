# README #

Tools for Jeol EB

## What is this repository for? ##

* Collections of tools for JEOL EB writer

## How do I get set up? ##

* Just clone repository and copy scripts to desired folders
* DXFtoJ01.py requires [dxfgrabber](https://pypi.org/project/dxfgrabber/), so
 
     $ pip install dxfgrabber

## Brief Description of Scripts ##

### Pattern Generator ###

* hexarray3-1.pl: build J01 format (and VectorScipt/AutoLisp) for triangular lattice of hexagonal holes

### Conversion from/to J01 ###

* J01toAL.pl: J01 to AutoCAD autolisp
* J01toVS.pl: J01 to VectorScript
* VStoJ01.pl: J01 from VectorScript
* DXFtoJ01.py: J01 from DXF

## Contribution guidelines ##

* Writing tests
* Code review
* Other guidelines

## Who do I talk to? ##

* Repo owner or admin
* Other community or team contact

## To Do ##

* Check programs
* dxfgrabber is deprecated, so rewrite using [ezdxf](https://pypi.org/project/ezdxf/)
* DXFtoJ01.py
 * support circle and arcs for RG command 