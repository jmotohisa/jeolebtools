#!/usr/local/bin/perl
open(OUT,">pattern.j01") || die "cannot open file\n";
# VStoJ01 version 0.2c 2006/09/19
#	'chop' is replaced with 'chomp'
# VStoJ01 version 0.2b 2006/09/09
#	modified to work with "BeginPoly" "EndPoly" mode
#	In "BeginPoly" "EndPoly", OpenPoly and ClosePoly is determined by the first and end points
#	"MoveTo" "LineTo" is converted to Poly unless PenSize=1
#
# VStoJ01 version 0.2a 2006/09/08
#	modified to work with OpenPoly with line thickness (pensize) option
#
# VStoJ01 version 0.1c 2003/1/23
# Convert VectorWorks data to JEOL JD1 Format
# commands supported:
#  RT, TK, LN
#  RG (from ver May 17, 2002)
#
foreach $aFile (@ARGV) {
#
	print "File Conversion: VectorSciprt -> JEOL 01 format\n";
	print "version 0.1c 2003/1/23 by J. Motohsia\n\n";
#
	print "VectorScript file name: $aFile\n";
	print "conversion factor (default =10) ? ";
	$cnvfactor= <STDIN>;
	chomp $cnvfactor;
	if(length($cnvfactor)==0) {
		$cnvfactor = 10;
	}
	print "magnification (default = 1) ? ";
	$mag = <STDIN>;
	chomp $mag;
	if(length($mag)==0) {
		$mag = 1;
	}
	$cnvfactor *= $mag;
# print "$mag,$cnvfactor\n";
	print "chip size shift x ? ";
	$shiftx = <STDIN>;
	chomp $shiftx;
	print "chip size shift y ? ";
	$shifty = <STDIN>;
	chomp $shifty;
#
	open(IN,"$ARGV[0]");
	$lineno=0;
	$ClosePoly=0;
	$PolyMode=0;
	@PolyModePoly=();
	while($line=<IN>) {
		$lineno++;
		&get_commands;
		&paraphrase_commands;
	}
}
close(OUT);

#
# paraphrase commands
sub paraphrase_commands {
	@commands = split(/;/,$command);
	foreach $aCommand (@commands)	{
		if($aCommand =~ /MoveTo\(([0-9---+,.]*)\)/) {
#
# MoveTo
#
#	&case_MoveTo($aCommand); # start of the line
#			$aCommand =~ /MoveTo\(([0-9---+.]*),([0-9---+.]*)\)/;
			@point=split(/,/,$1);
			if($PolyMode==0 && $pensize==1) {
				$line_start='';
				$line_xstart = xconv($point[0]);
				$line_ystart = yconv($point[1]);
#				print "$xstart,$ystart\n";
				$line_start = join(',',($line_xstart,$line_ystart));
			} else {
# poly mode
				@PolyModePoly=();
				push(@PolyModePoly,$point[0]);
				push(@PolyModePoly,$point[1]);
			}
		} elsif($aCommand =~ /LineTo\(([0-9---+,.]*)\)/) {
			@point=split(/,/,$1);
			if($PolyMode==0 && $pensize==1) {
				$line_xend = xconv($point[0]);
				$line_yend = yconv($point[1]);
				$line_end = join(',',($line_xend,$line_yend));
				$output = join('',('LN/0 ',$line_start,'-',$line_end));
				print OUT "$output\n";
				&check_boundary;
			} else {
				push(@PolyModePoly,$point[0]);
				push(@PolyModePoly,$point[1]);
				if($PolyMode==0) {
					&case_OpenPoly(join(",",@PolyModePoly));
					@PolyModePoly=();
				}
			}
		} elsif($aCommand =~ /Rect\(([0-9---+,.]*)\)/) {
#
# RT
#
			&case_Rect($1);
		} elsif($aCommand =~ /Poly\(([0-9---+,.]*)\)/) {
#
# TK (polygon)
#			print STDOUT "$1\n";
			if($ClosePoly==1) {
				&case_ClosePoly($1);
			} else {
				&case_OpenPoly($1);
			}
		} elsif($aCommand =~ /Arc\(([0-9---+,.\#\'\"�� ]*)\)/) {
#
# RG (Ring)
#
			&case_Arc($1);
		} elsif($aCommand =~ /Layer\(\'([a-zA-Z0-9_]*)\'\)/) {
#
# layer (insert comment line)
#
			print "* Layer:$1\n";
			print OUT "* Layer:$1\n";
		} elsif($aCommand =~ /ClosePoly/) {
			$ClosePoly = 1;
		} elsif($aCommand =~ /OpenPoly/) {
			$ClosePoly = 0;
		} elsif($aCommand =~ /PenSize\(([0-9.]*)\)/) {
			$pensize = $1;
		} elsif($aCommand =~ /BeginPoly/) {
			$PolyMode=1;
		} elsif($aCommand =~ /EndPoly/) {
			$PolyMode=0;
			$check=&check_firstend;
			if($check==1) {
				&case_ClosePoly(join(",",@PolyModePoly));
			} else {
				&case_OpenPoly(join(",",@PolyModePoly));
			}
			@PolyModePoly = ();
		}
	}
}

sub check_firstend {
	if(absolute($PolyModePoly[0]-$PolyModePoly[$#PolyModePoly-2]) <= 1e-3 && absolute($PolyModePoly[1]-$PolyModePoly[$#PolyModePoly-1]) <1e-3) {
# assume it is a close poly
		pop(@PolyModePoly);
		pop(@PolyModePoly);
		1;
	} else {
		0;
	}
}


# subroutine to get command(s)
sub get_commands {
	chomp $line;
	@lines = ();
# skip comment line {...}
	if($line =~ /(\{[^}]*\})/) {
		$command = $`;
#		push(@lines,$command);
		$command =~ s/\s//g;
		if(length($command)!=0 && $command !~ /;$/) {
			print STDERR "Syntax error at line $lineno.";
			print STDERR " cmd is $command\n";
			$command = "";
		}
		return;
	}
#
	$line =~ s/\s//g;
	if($line =~ /;$/) {
		$command=$line;
	} elsif(length($line)==0) {
		$command= "";
	} else {
#		print STDOUT "reading next line $line\n";
		push(@lines,$line);
#		do {
#			$line = <IN>;
#			$lineno++;
#			chop $line;
#			$line =~ s/\s//g;
#			push(@lines,$line);
#		} until($line =~ /;$/);
		while($line=<IN>) {
			$lineno++;
			chop $line;
			$line =~ s/\s//g;
			push(@lines,$line);
			if($line =~ /;$/) {
				last;
			}
		}
		$command = join('',@lines);
#  print STDOUT "$command\n\n";
	}
}

sub check_boundary {
	if($xstart<0 || $xstart >64000 || $ystart<0 || $ystart>64000 ||
				$xend<0 || $xend >64000 || $yend<0 || $yend>64000) {
			print STDERR "larger than the drawing area ! \n";
			print STDERR "$aCommand\n";
			print STDERR "$xstart, $ystart, $xend, $yend\n";
	}
}

sub get_min {
	$min = 100000;
	foreach $test (@_) {
		$min = $test if $test < $min;
	}
	$min;
}

sub absolute {
	local($abs);
	$abs = $_[0];
	if($abs < 0) {
		$abs = -$abs;
	}
	$abs;
}

sub xconv {
	int(($_[0]+$shiftx)*$cnvfactor+0.5);
}

sub yconv{
	int(($shifty-$_[0])*$cnvfactor+0.5);
}

sub case_MoveTo {
}

sub case_LineTo {
}

sub case_Rect {
	local($x1,$y1,$x11,$y11);
	@point=split(/,/,$_[0]);
	$xstart = xconv($point[0]);
	$ystart = yconv($point[1]);
	$xend = xconv($point[2]);
	$yend = yconv($point[3]);
	&check_boundary;
	$x1 = get_min($xstart,$xend);
	$y1 = get_min($ystart,$yend);
	$x11 = absolute($xstart-$xend);
	$y11 = absolute($ystart-$yend);
	print OUT "RT/0 $x1,$y1,$x11,$y11\n";
}

sub case_ClosePoly {
	local(@xy,@points,$ipoints,$xpt,$ypt);
	@xy = split(/,/,$_[0]);
	@xpts = ();
	@ypts = ();
	@points = ();
	$ipoints = 0;

	while(@xy)	{
		$xpt = xconv(shift(@xy));
		$ypt = yconv(shift(@xy));
		push(@xpts,$xpt);
		push(@ypts,$ypt);
		push(@points,"$xpt,$ypt");
	}
	$check = &check_clockwize;
	if($check == 0) {
		print OUT "TK/0 ";
		print OUT join('-',@points);
		print OUT "\n";
	} elsif($check == 1) {
		print OUT "TK/0 ";
		print OUT join('-',reverse(@points));
		print OUT "\n";
	} elsif($check == -1) {
		print OUT "* cannot check TK rotation direction\n";
		print OUT "*TK/0 ";
		print OUT join('-',@points);
		print OUT "\n*or\n*TK/0 ";
		print OUT join('-',reverse(@points));
		print OUT "\n";
	}
#	print STDOUT join('-',@points);
}

sub case_OpenPoly{
	local($cs,$sn);
	@xy = split(/,/,$_[0]);
	@xpts = ();
	@ypts = ();
	@points = ();
	$ipoints = 0;
	if($pensize==1) {
# line
		while(@xy)	{
			$xpt = xconv(shift(@xy));
			$ypt = yconv(shift(@xy));
			push(@xpts,$xpt);
			push(@ypts,$ypt);
			push(@points,"$xpt,$ypt");
		}
		print OUT "LN/0 ";
		print OUT join('-',@points);
		print OUT "\n";
	} else {
# convert to cloespoly with appropriate line thickening
		$x10=shift(@xy);
		$y10=shift(@xy);
		@points=();
		while(@xy) {
			$x20=shift(@xy);
			$y20=shift(@xy);
			$theta=atan2($y20-$y10,$x20-$x10);
			$cs=cos($theta);
			$sn=sin($theta);
#			$theta2=$theta/3.1415*180;
#			print "$theta2\n";
			push(@points,$x10+$pensize/2*(-$cs-$sn));
			push(@points,$y10+$pensize/2*( $cs-$sn));
			push(@points,$x20+$pensize/2*( $cs-$sn));
			push(@points,$y20+$pensize/2*( $cs+$sn));
			push(@points,$x20+$pensize/2*( $cs+$sn));
			push(@points,$y20+$pensize/2*(-$cs+$sn));
			push(@points,$x10+$pensize/2*(-$cs+$sn));
			push(@points,$y10+$pensize/2*(-$cs-$sn));
			&case_ClosePoly(join(",",@points));
			$x10=$x20;
			$y10=$y20;
			@points=();
		}
	}
}

sub check_clockwize {
	local($angle);
	$angle = calculate_angle(($xpts[0]+$xpts[$#xpts])/2,($ypts[0]+$ypts[$#ypts])/2);
	if($angle > 0.0001) {
		0;
	} elsif($angle <-0.0001) {
		1;
	} else {
		-1;
	}
}

sub calculate_angle {
	local($x_g,$y_g)=@_;
	local($angle,$i,$vx1,$vy1,$vx2,$vy2);
	$angle = 0;
#	print "$x_g, $y_g\n";
	for($i=1;$i<=$#xpts;$i++) {
		$vx1 = $xpts[$i-1] - $x_g;
		$vy1 = $ypts[$i-1] - $y_g;
		$vx2 = $xpts[$i] - $x_g;
		$vy2 = $ypts[$i] - $y_g;
		$angle += atan2($vx1*$vy2-$vx2*$vy1,$vx1*$vx2+$vy1*$vy2);
#		print "($vx1,$vy1)";
	}
#	$vx1 = $xpts[$#xpts] - $x_g;
#	$vy1 = $ypts[$#ypts] - $y_g;
#	print "($vx1,$vy1)\n";
#	$vx2 = $xpts[0] - $x_g;
#	$vy2 = $ypts[0] - $y_g;
#	$angle += atan2($vx1*$vy2-$vx2*$vy1,$vx1*$vx2+$vy1*$vy2);
	$angle = $angle/3.141592;
#	print "TK $angle\n";
#	$angle;
}

sub case_Arc {
#
# not complete, only circle
#
	local($xc,$yc,$r2,$ai,$af,$x1,$y1,$x2,$y2);
	@xy = split(/,/,$_[0]);
	$x1 = shift(@xy);
	$y1 = shift(@xy);
	$x2 = shift(@xy);
	$y2 = shift(@xy);
	$xc = xconv(($x1+$x2)/2);
	$yc = yconv(($y1+$y2)/2);
	$r2 = (xconv($x2)-xconv($x1))/2;
#
	$ai=0;
	$af=360;
#
	print OUT "RG/0 $xc,$yc,0,$r2,0,45\n";
	print OUT "RG/0 $xc,$yc,0,$r2,45,90\n";
	print OUT "RG/0 $xc,$yc,0,$r2,90,135\n";
	print OUT "RG/0 $xc,$yc,0,$r2,135,180\n";
	print OUT "RG/0 $xc,$yc,0,$r2,180,225\n";
	print OUT "RG/0 $xc,$yc,0,$r2,225,270\n";
	print OUT "RG/0 $xc,$yc,0,$r2,270,315\n";
	print OUT "RG/0 $xc,$yc,0,$r2,315,360\n";
}

